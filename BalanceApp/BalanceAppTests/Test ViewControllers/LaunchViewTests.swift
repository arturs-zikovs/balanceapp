//
//  LaunchViewTests.swift
//  BalanceAppTests
//
//  Created by Artur Zykov on 25/03/2020.
//  Copyright © 2020 Artur Zykov. All rights reserved.
//

import XCTest

@testable import BalanceApp
class LaunchViewTests: XCTestCase {
    
    var sut: LaunchVC!

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        sut = Storyboards.viewController(in: .main)
        _ = sut.view
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    // MARK: Initialization
    
    func testInit_LaunchVC_ReturnsVC() {
        
        XCTAssertNotNil(sut)
    }
    
    // MARK: Transition handling
    
    func testTransition_OnInitialLoad_ReturnsEvaluationState() {
        
        let launchVCMock = LaunchVCMock()
        let transitionType = launchVCMock.model.requestForNextTransition()
        XCTAssertEqual(transitionType, .evaluation)
    }
}
