//
//  ServiceTestsMocks.swift
//  BalanceAppTests
//
//  Created by Artur Zykov on 26/03/2020.
//  Copyright © 2020 Artur Zykov. All rights reserved.
//

import Foundation
import RealmSwift

@testable import BalanceApp
extension WheelEvaluationServiceTests {
    
    class RealmManagerMock: RealmManager {
        
        public static func deleteAllData(realm: Realm) {
            do {
                try realm.write {
                    realm.deleteAll()
                }
            } catch {
                print("Could not write to database: ", error)
            }
        }
    }
}

extension ActionPlanningServiceTests {
    
    class RealmManagerMock: RealmManager {
        
        public static func deleteAllData(realm: Realm) {
            do {
                try realm.write {
                    realm.deleteAll()
                }
            } catch {
                print("Could not write to database: ", error)
            }
        }
    }
}
