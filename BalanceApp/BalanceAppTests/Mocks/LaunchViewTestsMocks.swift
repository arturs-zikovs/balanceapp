//
//  LaunchViewTestsMocks.swift
//  BalanceAppTests
//
//  Created by Artur Zykov on 25/03/2020.
//  Copyright © 2020 Artur Zykov. All rights reserved.
//

import Foundation

@testable import BalanceApp
extension LaunchViewTests {

    class LaunchVCMock: LaunchVC {
        var model: LaunchVM = LaunchVM()
    }
}
