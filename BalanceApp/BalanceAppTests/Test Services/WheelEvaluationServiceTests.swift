//
//  WheelEvaluationServiceTests.swift
//  BalanceAppTests
//
//  Created by Artur Zykov on 24/03/2020.
//  Copyright © 2020 Artur Zykov. All rights reserved.
//

import XCTest

@testable import BalanceApp
class WheelEvaluationServiceTests: XCTestCase {
    
    //Properties
    var businessSection: WheelSegment = BalanceWheelGenerator.getSegment(withType: .career)
    var socialSection: WheelSegment = BalanceWheelGenerator.getSegment(withType: .social)
    var personalSection: WheelSegment = BalanceWheelGenerator.getSegment(withType: .personalGrowth)
    
    var sut: WheelEvaluationService!
    
    // MARK: Test config

    override func setUp() {
        
        RealmManagerMock.deleteAllData(realm: RealmManager.realm)
        sut = WheelEvaluationService()
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    // MARK: Initialization
    
    func testInit_WheelEvaluationServiceWithDefaultSegments() {
        
        XCTAssertNotNil(sut)
        XCTAssertEqual(sut.allSegments.count, 8)
    }
    
    func testInit_Segments_ContainsEmptyRatings() {
        
        let sectionRatings = sut.allSegments.compactMap { $0.rating == 0 ? $0.rating : nil }
        XCTAssertEqual(sectionRatings.count, 8)
    }
    
    // MARK: Section management
    
    func testValidate_CheckSection_ReturnsSection() {
        
        let section = sut.section(with: businessSection.title)
        XCTAssertEqual(section?.title, businessSection.title)
    }
    
    func testValidate_CheckMissingSection_ReturnsNoSection() {
        
        let section = sut.section(with: "No such section")
        XCTAssertNil(section)
    }
    
    // MARK: Correct section highlight
    
    func testSegments_HighlightLowestRatings_ReturnsSectionWithLowestRating() {
        
        sut.rateSection(with: businessSection.title, byApplyingRating: 6)
        sut.rateSection(with: socialSection.title, byApplyingRating: 1)
        sut.rateSection(with: personalSection.title, byApplyingRating: 7)
        
        let lowRatingSegments = sut.getSegmentsWithLowRating()
        XCTAssertEqual(lowRatingSegments.count, 6)
        
        let section = lowRatingSegments.sorted(by: >).first
        XCTAssertNotNil(section)
        XCTAssertEqual(section?.title, socialSection.title)
    }
    
    func testSegments_HighlightLowestRatings_ReturnsMultipleSegmentsWithLowestRating() {
        
        sut.rateSection(with: businessSection.title, byApplyingRating: 8)
        sut.rateSection(with: socialSection.title, byApplyingRating: 4)
        sut.rateSection(with: personalSection.title, byApplyingRating: 1)
        
        let lowRatingSegments = sut.getSegmentsWithLowRating()
        XCTAssertEqual(lowRatingSegments.count, 7)
        
        let notEmptySections = lowRatingSegments.filter { $0.rating != 0 }.sorted(by: <)
        let section = notEmptySections.first
        XCTAssertNotNil(section)
        XCTAssertEqual(notEmptySections.count, 2)
        XCTAssertEqual(section?.title, personalSection.title)
    }
    
    // MARK: Wheel evaluation
    
    func testModify_RateSection_ReturnsRatedSection() {
        
        rateSocialSection()
        let section = sut.section(with: socialSection.title)
        
        XCTAssertEqual(section?.rating, 4)
    }
    
    func testEvaluate_CheckEvaluation_ReturnsWheelEvaluationResult() {
        
        let canFinish = sut.canCompleteWheelEvaluation()
        XCTAssertFalse(canFinish)
        
        _ = evaluateWheel()
        XCTAssertEqual(sut.canCompleteWheelEvaluation(), true)
    }
    
    func testEvaluate_CheckOneSegmentEvaluation_ReturnsWheelEvaluationFalse() {
        
        rateSocialSection()
        let canFinish = sut.canCompleteWheelEvaluation()
        XCTAssertFalse(canFinish)
    }
    
    func testEvaluate_FinishEvaluation_ReturnsEvaluatedWheel() {
        
        let segments = evaluateWheel()
        let evaluatedWheel = sut.lastEvaluatedWheel()
        
        XCTAssertNotNil(evaluatedWheel)
        XCTAssertEqual(evaluatedWheel?.segments.count, segments.count)
        
        let isEvaluated = evaluatedWheel?.segments.reduce(true) { $1.isRated ? $0 : $1.isRated }
        XCTAssertEqual(isEvaluated, true)
    }
    
    func testEvaluate_TwoWheelEvaluation_ReturnsLastEvaluatedWheel() {
        
        _ = evaluateWheel()
        
        sut = WheelEvaluationService(wheel: BalanceWheelGenerator.defaultWheel())
        _ = evaluateWheel(with: 4)
        
        let evaluatedWheel = sut.lastEvaluatedWheel()
        XCTAssertEqual(evaluatedWheel?.segments.first?.rating, 4)
    }
    
    // MARK: Important segment selection
    
    func testSelect_WheelSegment_ReturnsSelectedSegment() {
        let segments = evaluateWheel()
        
        let anySegment = segments.randomElement()!
        sut.selectForPlanning(segment: anySegment)
        
        let wheel = sut.lastEvaluatedWheel()
        XCTAssertNotNil(wheel?.selectedSegment)
    }
    
    // MARK: Support methods
    
    private func evaluateWheel(with rating: Int = 7) -> [WheelSegment] {
        let segments = BalanceWheelGenerator.defaultWheel().segments
        segments.forEach { sut.rateSection(with: $0.title, byApplyingRating: rating) }
        sut.completeWheelEvaluation()
        
        return segments.toArray()
    }
    
    private func rateSocialSection() {
        sut.rateSection(with: socialSection.title, byApplyingRating: 4)
    }
}
