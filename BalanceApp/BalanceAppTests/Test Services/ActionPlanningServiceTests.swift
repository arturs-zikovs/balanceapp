//
//  ActionPlanningServiceTests.swift
//  BalanceAppTests
//
//  Created by Artur Zykov on 30/03/2020.
//  Copyright © 2020 Artur Zykov. All rights reserved.
//

import XCTest

@testable import BalanceApp
class ActionPlanningServiceTests: XCTestCase {
    
    // Properties
    var sut: ActionPlanningService!
    
    let funActionItem = ActionPlanItem(title: "Make fun", expirationDate: Date())
    let makeFriendsActionItem = ActionPlanItem(title: "Make friends", expirationDate: Date())
    let cleanHouseActionItem = ActionPlanItem(title: "Clean house", expirationDate: Date())
    let doCrazyActionItem = ActionPlanItem(title: "Do something crazy", expirationDate: Date())
    
    // MARK: Configuration

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        RealmManagerMock.deleteAllData(realm: RealmManager.realm)
        preparePreconditions()
    }
    
    func preparePreconditions() {
        
        let wheelService = WheelEvaluationService()
        let segments = BalanceWheelGenerator.defaultWheel().segments
        segments.forEach { wheelService.rateSection(with: $0.title, byApplyingRating: 4) }
        wheelService.completeWheelEvaluation()
        wheelService.selectForPlanning(segment: segments.randomElement()!)
        
        let wheel = wheelService.lastEvaluatedWheel()!
        let selectedSegment = wheel.selectedSegment!
        
        sut = ActionPlanningService(wheel: wheel, segment: selectedSegment)
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    // MARK: Initialization
    
    func testInit_ActionPlanningServiceWithWheelAndSegment() {
        
        XCTAssertNotNil(sut)
    }
    
    // MARK: Add new action items to action plan
    
    func testAdd_ActionItemToPlan_CanAdd() {
        
        sut.addActionItem(funActionItem)
        XCTAssertEqual(sut.actions.count, 1)
    }
    
    func testAdd_FourActionItemsToPlan_UnableToAddFourth() {
        
        sut.addActionItem(funActionItem)
        sut.addActionItem(makeFriendsActionItem)
        sut.addActionItem(cleanHouseActionItem)
        sut.addActionItem(doCrazyActionItem)
        XCTAssertEqual(sut.actions.count, 3)
    }
    
    // MARK: Create action plan
    
    func testCreate_EmptyActionPlan_ReturnsNil() {
        
        let actionPlan = sut.confirmActionPlan()
        XCTAssertNil(actionPlan)
    }
    
    func testCreate_ActionPlan_ReturnsActionPlan() {
        
        sut.addActionItem(makeFriendsActionItem)
        sut.addActionItem(cleanHouseActionItem)
        
        let actionPlan = sut.confirmActionPlan()
        XCTAssertNotNil(actionPlan)
        XCTAssertEqual(actionPlan?.items.count, 2)
    }
    
//    func testCreate_ActionPlan_ReturnsActionPlan() {
//
//        sut.addActionItem(makeFriendsActionItem)
//        sut.addActionItem(cleanHouseActionItem)
//
//        let actionPlan = sut.confirmActionPlan()
//        XCTAssertNotNil(actionPlan)
//        XCTAssertEqual(actionPlan?.items.count, 2)
//    }
}
