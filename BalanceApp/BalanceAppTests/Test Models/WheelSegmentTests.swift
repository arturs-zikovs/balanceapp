//
//  WheelSegmentTests.swift
//  WheelSegmentTests
//
//  Created by Artur Zykov on 24/03/2020.
//  Copyright © 2020 Artur Zykov. All rights reserved.
//

import XCTest

@testable import BalanceApp
class WheelSegmentTests: XCTestCase {
    
    // MARK: Test config

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    // MARK: Initialization
    
    func testInit_WheelSegmentWithTitle() {
        let testSection = WheelSegment(title: "Friends")
        
        XCTAssertNotNil(testSection)
        XCTAssertEqual(testSection.title, "Friends")
    }
    
    func testInit_WheelSegmentWithTitleAndRating() {
        let testSection = WheelSegment(title: "Friends", rating: 7)
        
        XCTAssertNotNil(testSection)
        XCTAssertEqual(testSection.title, "Friends")
        XCTAssertEqual(testSection.rating, 7)
    }
}
