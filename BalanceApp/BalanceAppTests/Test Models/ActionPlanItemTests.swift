//
//  ActionPlanItemTests.swift
//  BalanceAppTests
//
//  Created by Artur Zykov on 25/03/2020.
//  Copyright © 2020 Artur Zykov. All rights reserved.
//

import XCTest

@testable import BalanceApp
class ActionPlanItemTests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    // MARK: Initialization
    
    func testInit_ActionPlanItemWithTitleAndExpirationDate() {
        
        let sut = ActionPlanItem(title: "Make new friends",
                                 expirationDate: Date())
        
        XCTAssertNotNil(sut)
    }

}
