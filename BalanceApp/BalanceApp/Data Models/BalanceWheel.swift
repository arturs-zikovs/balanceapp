//
//  BalanceWheel.swift
//  BalanceApp
//
//  Created by Artur Zykov on 25/03/2020.
//  Copyright © 2020 Artur Zykov. All rights reserved.
//

import Foundation
import RealmSwift

class BalanceWheel: Object {
    
    //Properties
    var segments = List<WheelSegment>()
    @objc dynamic var selectedSegment: WheelSegment?
    @objc dynamic var creationDate = Date()
    
    // MARK: Initialization
    
    convenience required init(segments: [WheelSegment]) {
        self.init()
        
        self.segments.append(objectsIn: segments)
    }
}

// MARK: Comparable protocol

extension BalanceWheel: Comparable {
    
    static func < (lhs: BalanceWheel, rhs: BalanceWheel) -> Bool {
        lhs.creationDate < rhs.creationDate
    }
}
