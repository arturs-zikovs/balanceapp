//
//  ActionPlan.swift
//  BalanceApp
//
//  Created by Artur Zykov on 30/03/2020.
//  Copyright © 2020 Artur Zykov. All rights reserved.
//

import Foundation
import RealmSwift

class ActionPlan: Object {
    
    //Properties
    var items = List<ActionPlanItem>()
    
    // MARK: Initialization
    
    convenience required init(items: [ActionPlanItem]) {
        self.init()
        
        self.items.append(objectsIn: items)
    }
}
