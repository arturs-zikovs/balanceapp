//
//  WheelSegment.swift
//  BalanceApp
//
//  Created by Artur Zykov on 24/03/2020.
//  Copyright © 2020 Artur Zykov. All rights reserved.
//

import Foundation
import RealmSwift

class WheelSegment: Object {
    
    //Properties
    @objc dynamic var title: String = ""
    @objc dynamic var rating: Int = 0 {
        didSet {
            isRated = true
        }
    }
    @objc dynamic var isRated = false
    
    // MARK: Initialization
    
    convenience required init(title: String, rating: Int = 0) {
        self.init()
        
        self.title = title
        self.rating = rating
    }
}

// MARK: Comparable protocol

extension WheelSegment: Comparable {
    
    static func < (lhs: WheelSegment, rhs: WheelSegment) -> Bool {
        lhs.rating < rhs.rating
    }
}
