//
//  ActionPlanItem.swift
//  BalanceApp
//
//  Created by Artur Zykov on 25/03/2020.
//  Copyright © 2020 Artur Zykov. All rights reserved.
//

import Foundation
import RealmSwift

class ActionPlanItem: Object {
    
    //Properties
    @objc dynamic var title: String = ""
    @objc dynamic var expirationDate: Date = Date()
    
    // MARK: Initialization
    
    convenience required init(title: String, expirationDate: Date) {
        self.init()
        
        self.title = title
        self.expirationDate = expirationDate
    }
}
