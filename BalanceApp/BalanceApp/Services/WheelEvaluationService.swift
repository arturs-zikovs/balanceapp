//
//  WheelEvaluationService.swift
//  BalanceApp
//
//  Created by Artur Zykov on 24/03/2020.
//  Copyright © 2020 Artur Zykov. All rights reserved.
//

import Foundation
import RealmSwift

class WheelEvaluationService {
    
    // Properties
    private var balanceWheel: BalanceWheel?
    public var allSegments: [WheelSegment] {
        return balanceWheel?.segments.toArray() ?? []
    }
    
    // MARK: Initialization
    
    init(wheel balanceWheel: BalanceWheel? = nil) {
        
        self.balanceWheel = balanceWheel
        loadSegments()
    }
    
    // MARK: Data fetching
    
    private func loadSegments() {
        
        guard balanceWheel != nil else {
            let lastBalanceWheel = RealmManager.realm.objects(BalanceWheel.self).sorted(by: >).last
            
            if let lastBalanceWheel = lastBalanceWheel {
                self.balanceWheel = lastBalanceWheel
            } else {
                self.balanceWheel = BalanceWheelGenerator.defaultWheel()
            }
            
            return
        }
    }
    
    func section(with title: String) -> WheelSegment? {
        return allSegments.first { $0.title == title }
    }
    
    func lastEvaluatedWheel() -> BalanceWheel? {
        return RealmManager.realm.objects(BalanceWheel.self).sorted(by: >).first
    }
    
    // MARK: Section rating modifications
    
    func rateSection(with title: String, byApplyingRating rating: Int) {
        let segment = allSegments.first(where: { $0.title == title })
        
        segment?.rating = rating
    }
    
    func getSegmentsWithLowRating() -> [WheelSegment] {
        
        let sectionRatings = allSegments.map { $0.rating }
        let ratingLimit = LowestRatingCalculator.calculateRatingLimit(for: sectionRatings)
        
        return allSegments.filter { $0.rating <= ratingLimit }
    }
    
    // MARK: Evaluate wheel
    
    func canCompleteWheelEvaluation() -> Bool {
        return allSegments.reduce(true) { $1.isRated ? $0 : $1.isRated }
    }
    
    func completeWheelEvaluation() {
        guard let balanceWheel = balanceWheel else { return }
        
        RealmManager.write(realm: RealmManager.realm) {
            RealmManager.realm.add(balanceWheel)
        }
    }
    
    // MARK: Select for planning
    
    func selectForPlanning(segment: WheelSegment) {
        let wheel = lastEvaluatedWheel()
        
        RealmManager.write(realm: RealmManager.realm) {
            wheel?.selectedSegment = segment
        }
    }
}
