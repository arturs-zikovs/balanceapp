//
//  ActionPlanningService.swift
//  BalanceApp
//
//  Created by Artur Zykov on 30/03/2020.
//  Copyright © 2020 Artur Zykov. All rights reserved.
//

import Foundation

class ActionPlanningService {
    
    //Properties
    private let wheel: BalanceWheel
    private let activeSegment: WheelSegment
    
    private var actionItems: [ActionPlanItem] = []
    public var actions: [ActionPlanItem] {
        return actionItems
    }
    
    // MARK: Initialization
    
    init(wheel: BalanceWheel, segment: WheelSegment) {
        self.wheel = wheel
        self.activeSegment = segment
    }
    
    // MARK: Append new action
    
    func addActionItem(_ actionItem: ActionPlanItem) {
        if actionItems.count < 3 {
            actionItems.append(actionItem)
        }
    }
    
    // MARK: Action plan creation
    
    func confirmActionPlan() -> ActionPlan? {
        guard actionItems.count > 0 else { return nil }
        
        let plan = ActionPlan(items: actionItems)
        return plan
    }
}
