//
//  RealmManager.swift
//  BalanceApp
//
//  Created by Artur Zykov on 25/03/2020.
//  Copyright © 2020 Artur Zykov. All rights reserved.
//

import Foundation
import RealmSwift

extension Results {
    
    func toArray() -> [Element] {
        return compactMap { $0 }
    }
}

public class RealmManager {

    static var realm: Realm {
        get {
            do {
                let config = Realm.Configuration(deleteRealmIfMigrationNeeded: true)
                Realm.Configuration.defaultConfiguration = config
                
                let realm = try Realm()
                return realm
            }
            catch {
                print("Could not access database: ", error)
            }
            return self.realm
        }
    }

    public static func write(realm: Realm, writeClosure: () -> ()) {
        do {
            try realm.write {
                writeClosure()
            }
        } catch {
            print("Could not write to database: ", error)
        }
    }
}
