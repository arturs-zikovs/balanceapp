//
//  RealmCollectionExtensions.swift
//  BalanceApp
//
//  Created by Artur Zykov on 25/03/2020.
//  Copyright © 2020 Artur Zykov. All rights reserved.
//

import Foundation
import RealmSwift

extension RealmCollection {
    
    func toArray<T>() -> [T] {
        return self.compactMap{ $0 as? T }
    }
}
