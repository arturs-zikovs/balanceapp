//
//  LaunchVM.swift
//  BalanceApp
//
//  Created by Artur Zykov on 25/03/2020.
//  Copyright © 2020 Artur Zykov. All rights reserved.
//

import Foundation

class LaunchVM {
    
    enum TransitionFlow: String {
        case evaluation = "PresentEvaluationFlow"
        case planning = "PresentPlanningFlow"
    }
    
    // MARK: Transition management
    
    func requestForNextTransition() -> TransitionFlow {
        
        // TODO: Add transition logic here
        return .evaluation
    }
}

