//
//  LaunchVC.swift
//  BalanceApp
//
//  Created by Artur Zykov on 24/03/2020.
//  Copyright © 2020 Artur Zykov. All rights reserved.
//

import UIKit

class LaunchVC: UIViewController {
    
    // Properties
    private lazy var model: LaunchVM = LaunchVM()
    
    // MARK: View lifecycle
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        performNextTransition()
    }
    
    // MARK: Transition management
    
    func performNextTransition() {
        transitionTo(flow: model.requestForNextTransition())
    }
    
    private func transitionTo(flow: LaunchVM.TransitionFlow) {
        
        let flowName = flow.rawValue
        performSegue(withIdentifier: flowName,
                     sender: nil)
    }
}

