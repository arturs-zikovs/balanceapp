//
//  EvaluationWheelVC.swift
//  BalanceApp
//
//  Created by Artur Zykov on 25/03/2020.
//  Copyright © 2020 Artur Zykov. All rights reserved.
//

import UIKit

class EvaluationWheelVC: UIViewController {
    
    // Properties
    private var model: EvaluationWheelVM = EvaluationWheelVM()
    
    // MARK: View lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        model.service.completeWheelEvaluation()
    }
}

