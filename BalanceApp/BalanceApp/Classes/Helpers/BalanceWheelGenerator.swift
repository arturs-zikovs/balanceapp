//
//  BalanceWheelGenerator.swift
//  BalanceApp
//
//  Created by Artur Zykov on 25/03/2020.
//  Copyright © 2020 Artur Zykov. All rights reserved.
//

import Foundation

class BalanceWheelGenerator {
    
    enum DefaultSegmentType: String {
        case career = "Business/Career"
        case family = "Friends/Family"
        case finances = "Money/Finances"
        case social = "Social Life"
        case personalGrowth = "Personal Growth"
        case health = "Health"
        case leisure = "Fun/Leisure"
        case physicalEnvironment = "Physical Environment"
    }
    
    // MARK: Default wheel
    
    static func defaultWheel() -> BalanceWheel {
        
        let segments = [BalanceWheelGenerator.getSegment(withType: .career),
                        BalanceWheelGenerator.getSegment(withType: .family),
                        BalanceWheelGenerator.getSegment(withType: .finances),
                        BalanceWheelGenerator.getSegment(withType: .social),
                        BalanceWheelGenerator.getSegment(withType: .personalGrowth),
                        BalanceWheelGenerator.getSegment(withType: .health),
                        BalanceWheelGenerator.getSegment(withType: .leisure),
                        BalanceWheelGenerator.getSegment(withType: .physicalEnvironment)]
        
        return BalanceWheel(segments: segments)
    }
    
    static func getSegment(withType type: DefaultSegmentType) -> WheelSegment {
        return WheelSegment(title: type.rawValue)
    }
}
