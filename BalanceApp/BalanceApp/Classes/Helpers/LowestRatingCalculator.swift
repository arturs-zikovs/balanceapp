//
//  LowestRatingCalculator.swift
//  BalanceApp
//
//  Created by Artur Zykov on 24/03/2020.
//  Copyright © 2020 Artur Zykov. All rights reserved.
//

import Foundation

class LowestRatingCalculator {
    
    static func calculateRatingLimit(for ratings: [Int]) -> Int {
        var limit = 0
        
        if let lowestRating = ratings.sorted().first,
            let highestRating = ratings.sorted().last {
            
            limit = lowestRating + Int(ceil(Float(highestRating - lowestRating) / 2))
        }
        
        return limit
    }
}
