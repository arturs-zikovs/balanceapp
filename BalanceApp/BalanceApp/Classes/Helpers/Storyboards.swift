//
//  Storyboards.swift
//  BalanceApp
//
//  Created by Artur Zykov on 25/03/2020.
//  Copyright © 2020 Artur Zykov. All rights reserved.
//

import UIKit

class Storyboards {
    
    enum Name: String {
        case main = "Main"
        case evaluation = "Evaluation"
        case planning = "Planning"
    }
    
    // MARK: View controller fetching
    
    private static func viewController<VCClass: UIViewController>(name: String, in storyboardName: Name) -> VCClass? {
        let storyboard = UIStoryboard(name: storyboardName.rawValue, bundle: nil)
        return storyboard.instantiateViewController(identifier: name)
    }
    
    static func viewController<VCClass: UIViewController>(in storyboardName: Name) -> VCClass? {
        return viewController(name: String(describing: VCClass.self), in: storyboardName)
    }
}
